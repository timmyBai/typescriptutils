import  { Validate } from '@/utils/validate';

describe('test validate class', (): void => {
    /**
     * 測試全數字規則
     */
    test('test inspectNumber function', (): void => {
        const validate: Validate = new Validate();

        let text: string = '09';

        expect(validate.inspectNumber(text)).toBe(true);

        text = 'abc';
        
        expect(validate.inspectNumber(text)).toBe(false);
    });

    /**
     * 測試全小寫規則
     */
    test('test inspectLowerCase function', (): void => {
        const validate: Validate = new Validate();

        let text: string = 'abc';

        expect(validate.inspectLowerCase(text)).toBe(true);

        text = 'abC';

        expect(validate.inspectLowerCase(text)).toBe(false);
    });

    /**
     * 測試全大寫規則
     */
    test('test inspectUpperCase function', (): void => {
        let text: string = 'ABC';

        const validate: Validate = new Validate();

        expect(validate.inspectUpperCase(text)).toBe(true);

        text = 'abC';
        
        expect(validate.inspectUpperCase(text)).toBe(false);
    });

    /**
     * 測試身份證規則
     */
    test('test inspectIdenitiyNumber function', (): void => {
        let text: string = 'L12345678f';

        const validate: Validate = new Validate();

        expect(validate.inspectIdenitiyNumber(text)).toBe(false);

        text = 'L123456789f';

        expect(validate.inspectIdenitiyNumber(text)).toBe(false);

        text = 'L123456789';

        expect(validate.inspectIdenitiyNumber(text)).toBe(true);
    });

    /**
     * 測試電話號碼
     */
    test('test inspectPhoneNumber function', (): void => {
        let text: string = '098899815a';
        
        const validate: Validate = new Validate();
        
        expect(validate.inspectIdenitiyNumber(text)).toBe(false);

        text = '09889981515';

        expect(validate.inspectIdenitiyNumber(text)).toBe(false);

        text = '0988998151';

        expect(validate.inspectIdenitiyNumber(text)).toBe(false);
    });
});