export class Validate {
    /**
     * 檢查是否為數字
     * @param {string} text 驗證文字
     */
    public inspectNumber(text: string): boolean {
        const pattern: RegExp = /^[0-9]+$/g;
        
        return pattern.test(text);
    }

    /**
     * 檢查是否為小寫英文
     * @param {string} text 驗證文字
     */
    public inspectLowerCase(text: string): boolean {
        const pattern: RegExp = /^[a-z]+$/g;

        return pattern.test(text);
    }

    /**
     * 檢查是否為大寫英文
     * @param {string} text 驗證文字
     */
    public inspectUpperCase(text: string): boolean {
        const pattern: RegExp = /^[A-Z]+$/g;

        return pattern.test(text);
    }

    /**
     * 檢查身份證字號
     * @param {string} text 驗證文字
     */
    public inspectIdenitiyNumber(text: string): boolean {
        const pattern: RegExp = /^[A-Z]{1}[0-9]{9}$/g;

        return pattern.test(text);
    }

    /**
     * 檢查電話號碼
     * @param {string} text 驗證文字
     */
    public inspectPhoneNumber(text: string): boolean {
        const pattern: RegExp = /0([0-9]{1})([0-9]{8})$/g;
        
        return pattern.test(text);
    }
}